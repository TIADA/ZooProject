package animals;

public abstract class Bird extends Animal {

    public Bird(double size, String nickname) {
        super(size, nickname);
    }
}
