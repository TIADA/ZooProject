package animals;

public abstract class Felinae extends Predator {
    public Felinae(double size, String nickname) {
        super(size, nickname);
    }
}
