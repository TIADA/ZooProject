package animals;

public abstract class Mammal extends Animal {
    private int pregnantPeriod; //in monthes

    public Mammal(double size, String nickname) {
        super(size, nickname);
    }

    public int getPregnantPeriod() {
        return pregnantPeriod;
    }

    public void setPregnantPeriod(int pregnantPeriod) {
        this.pregnantPeriod = pregnantPeriod;
    }
}
