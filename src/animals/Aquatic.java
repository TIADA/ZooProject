package animals;

public abstract class Aquatic extends Mammal {
    public Aquatic(double size, String nickname) {
        super(size, nickname);
    }
}
