package animals;

import input.Input;

public class ForestWolf extends Cannine {
    public ForestWolf(double size, String nickname) {
        super(size, nickname);
        type = ForestWolf.class.getSimpleName();
    }

    public static ForestWolf createWolf() {
        double size = Input.megaInputNumber("Enter your wolf's size");
        String name = Input.megaInputString("Enter your wolf's name");
        return new ForestWolf(size, name);
    }

    @Override
    public void sound() {
        System.out.println("Woof");
    }

    @Override
    public double jump() {
        return 100 / getSize();
    }
}


