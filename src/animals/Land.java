package animals;

public abstract class Land extends Mammal {
    private int limbs;

    public Land(double size, String nickname) {
        super(size, nickname);
    }

    public int getLimbs() {
        return limbs;
    }

    public void setLimbs(int limbs) {
        this.limbs = limbs;
    }
}
