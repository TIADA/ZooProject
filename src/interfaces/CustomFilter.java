package interfaces;

import java.io.File;
import java.util.List;

public interface CustomFilter {
    void filter(String str, List <File> pathList);
}
