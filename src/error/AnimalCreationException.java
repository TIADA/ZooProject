package error;

public class AnimalCreationException extends Exception{
    public AnimalCreationException(String str) {
        super("Animal can't be created");
    }


}
