package error;

public class AnimalInvalidSizeException extends AnimalCreationException {
    public AnimalInvalidSizeException() {
        super("Wrong name");
    }
}
