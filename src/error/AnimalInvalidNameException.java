package error;

public class AnimalInvalidNameException extends AnimalCreationException {
    public AnimalInvalidNameException() {
        super("Your size is wrong");
    }
}
