package food;

public enum NameFood {
    CARROT(20), MEAT(50);
    private double energyValue = 10;

    NameFood(double energyValue) {
        this.energyValue = energyValue;
    }

    public double getEnergyValue() {
        return energyValue;
    }

}
