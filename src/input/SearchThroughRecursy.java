package input;

import interfaces.CustomFilter;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class SearchThroughRecursy implements CustomFilter {
    @Override
    public void filter(String str, List<File> pathList) {
        File file;
        Boolean result;
        if (str.length() != 0) {
            file = new File(str);
        } else {
            file = new File(".");
        }
        if (!file.exists()) {
            System.out.println("Your path is not exists.");
            return;
        }
        result = file.isDirectory();
        if (result) {
            File[] fileArr = file.listFiles();
            if (fileArr != null) {
                for (File aFileArr : fileArr) {
                    try {
                        filter(aFileArr.getCanonicalPath(), pathList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (file.getAbsolutePath().lastIndexOf(".csv") != -1) {
            pathList.add(file);
        }
    }
}
