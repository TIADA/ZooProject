package input;

import animals.Animal;
import com.sun.org.apache.bcel.internal.util.ClassPath;
import error.AnimalCreationException;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static main.Main.scan;

public class Input {
    public static double megaInputNumber(String str) {
        System.out.println(str);
        while (true) {
            String input = scan.nextLine();
            try {
                return Double.parseDouble(input.replace(",", "."));
            } catch (NumberFormatException e) {
                System.out.println("Please enter number");
            }
        }
    }

    public static String megaInputString(String str) {
        System.out.println(str);
        return scan.nextLine();
    }

    public static String inputFromConsole() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();
        int temp;
        try {
            while ((temp = br.read()) != -1 && temp != 10) {
                sb.append((char) temp);
            }
        } catch (IOException e) {
            System.out.println("This not gonna happen");
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static List<Animal> importFromFile(String st) throws AnimalCreationException {
        StringBuilder sb = new StringBuilder();
        List<Animal> listAnimal = new LinkedList<>();
        File file = new File(st);
        if (!file.exists()) {
            System.out.println("Your file is not exists.");
        }
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            int nextChar;
            int temp = 0;
            while ((nextChar = br.read()) != -1) {
                if (nextChar >= 32) {
                    sb.append((char) nextChar);
                } else if (nextChar == 10 && temp == 13) {
                    listAnimal.add(Animal.convertFromString(sb.toString()));
                    sb.setLength(0);
                }
                temp = nextChar;
            }
            if (sb.length() > 0) {
                listAnimal.add(Animal.convertFromString(sb.toString()));
                sb.setLength(0);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return listAnimal;
    }

    public static void searchingInCatalog(String str, List<String> pathList) {
        File file;
        Boolean result;
        if (str.length() != 0) {
            file = new File(str);
        } else {
            file = new File("\\");
        }
        if (!file.exists()) {
            System.out.println("Your path is not exists.");
            return;
        }
        result = file.isDirectory();
        if (result) {
            String[] strArr = file.list();
            if ((strArr != null ? strArr.length : 0) != 0) {
                for (int i = 0; i < strArr.length; i++){
                    searchingInCatalog(file.getAbsolutePath().concat("\\").concat(strArr[i]), pathList);
                }
            }
        } else if (file.getAbsolutePath().contains(".csv")){
            pathList.add(file.getAbsolutePath());
        }
    }
}

