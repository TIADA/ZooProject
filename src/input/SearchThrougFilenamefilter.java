package input;

import interfaces.CustomFilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.List;

public class SearchThrougFilenamefilter implements CustomFilter {
    @Override
    public void filter(String str, List<File> pathList) {
        File file;
        if (str.length() != 0) {
            file = new File(str);
        } else {
            file = new File(".");
        }
        if (!file.exists()) {
            System.out.println("Your path is not exists.");
            return;
        }
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File file, String name) {
                return (name.lastIndexOf(".csv") != -1);
            }
        };
        File[] fileArr = file.listFiles(filter);
        if (fileArr != null) {
            Collections.addAll(pathList, fileArr);
        } else {
            System.out.println("There is no such files");
        }
    }
}
