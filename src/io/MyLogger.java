package io;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyLogger {
    public static void log(String str) {
        Date date = new Date();
        Calendar c = new GregorianCalendar();
        DateFormat df = new SimpleDateFormat("dd_MM_YYYY");
        String fileName = "log_" + df.format(date) + ".txt";
        DateFormat timeFormat = new SimpleDateFormat("YYYY_MM_dd HH:mm:ss.SSS");
        String logTime = timeFormat.format(date);
        File file = new File(fileName);
        try (PrintWriter bw = new PrintWriter(new BufferedWriter(new FileWriter(file, true)))) {
            bw.println(logTime + " " + str);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
