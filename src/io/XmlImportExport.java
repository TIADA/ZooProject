package io;


import animals.*;
import main.ExtensibleCage;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class XmlImportExport  {
    final static String ELEMENT_NAME_ROOT = "ZOO";
    final static String ELEMENT_NAME_CAGE = "CAGE";
    final static String ELEMENT_NAME_ANIMAL = "ANIMAL";
    final static String ATRIBUTE_NAME_TYPE = "type";
    final static String ATRIBUTE_NAME_FILL = "fill";
    final static String ATRIBUTE_NAME_SIZE = "size";

    public static void exportToFile(Map<String, ExtensibleCage<? extends Animal>> cages) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement(ELEMENT_NAME_ROOT);
            doc.appendChild(rootElement);
            for (String key : cages.keySet()) {
                ExtensibleCage<? extends Animal> cage = cages.get(key);
                Element cageElement = doc.createElement(ELEMENT_NAME_CAGE);
                rootElement.appendChild(cageElement);
                Attr cageTypeAttribute = doc.createAttribute(ATRIBUTE_NAME_TYPE);
                cageTypeAttribute.setValue(key);
                cageElement.setAttributeNode(cageTypeAttribute);

                for (Animal animal : cage.getCage()) {
                    Element animalElement = doc.createElement(ELEMENT_NAME_ANIMAL);

                    Attr animalTypeAttribute = doc.createAttribute(ATRIBUTE_NAME_TYPE);
                    animalTypeAttribute.setValue(animal.getType());
                    animalElement.setAttributeNode(animalTypeAttribute);

                    Attr animalSizeAttribute = doc.createAttribute(ATRIBUTE_NAME_SIZE);
                    animalSizeAttribute.setValue(String.valueOf(animal.getSize()));
                    animalElement.setAttributeNode(animalSizeAttribute);

                    Attr animalFillAttribute = doc.createAttribute(ATRIBUTE_NAME_FILL);
                    animalFillAttribute.setValue(String.valueOf(animal.getFill()));
                    animalElement.setAttributeNode(animalFillAttribute);
                    animalElement.appendChild(doc.createTextNode(animal.getNickname()));
                    cageElement.appendChild(animalElement);
                }
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(".autosave"));
            transformer.transform(source, result);

        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());
        } catch (TransformerConfigurationException e) {
            System.out.println(e.getMessage());
        } catch (TransformerException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void loadFromXml(Map<String, ExtensibleCage<? extends Animal>> map) throws XmlImportException{
        File inputFile = new File(".autosave");
        if (!inputFile.exists()) {
            System.out.println("There is no saved animals");
            return;
        }
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            NodeList cageList = doc.getElementsByTagName(ELEMENT_NAME_CAGE);
            for (int i = 0; i < cageList.getLength(); i++) {
                Node cageNode = cageList.item(i);
                Element cageElement = (Element) cageNode;

                NodeList animalList = cageElement.getElementsByTagName(ELEMENT_NAME_ANIMAL);
                for (int j = 0; j < animalList.getLength(); j++) {
                    Node animalNode = animalList.item(j);
                    Element animalElement = (Element) animalNode;

                    switch (animalElement.getAttribute(ATRIBUTE_NAME_TYPE)) {
                        case "DomesticCat": {
                            Animal temp = new DomesticCat(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_SIZE)),
                                    animalElement.getTextContent());
                            temp.setFill(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_FILL)));
                            map.get(cageElement.getAttribute(ATRIBUTE_NAME_TYPE)).addAnimal(temp);
                            break;
                        }
                        case "ForestWolf": {
                            Animal temp = new ForestWolf(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_SIZE)),
                                    animalElement.getTextContent());
                            temp.setFill(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_FILL)));
                            map.get(cageElement.getAttribute(ATRIBUTE_NAME_TYPE)).addAnimal(temp);
                            break;
                        }
                        case "Rabbit": {
                            Animal temp = new Rabbit(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_SIZE)),
                                    animalElement.getTextContent());
                            temp.setFill(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_FILL)));
                            map.get(cageElement.getAttribute(ATRIBUTE_NAME_TYPE)).addAnimal(temp);
                            break;
                        }
                        case "Raven": {
                            Animal temp = new Raven(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_SIZE)),
                                    animalElement.getTextContent());
                            temp.setFill(Double.parseDouble(animalElement.getAttribute(ATRIBUTE_NAME_FILL)));
                            map.get(cageElement.getAttribute(ATRIBUTE_NAME_TYPE)).addAnimal(temp);
                            break;
                        }
                        default:{
                            continue;
                        }
                    }

                }
            }

        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());
        } catch (SAXException | IOException e) {
            System.out.println(e.getMessage());
        }
    }
    public class XmlImportException extends Exception{
        public XmlImportException(){
            super("There was some exception during import");
        }
    }
}



